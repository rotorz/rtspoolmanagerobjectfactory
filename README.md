# README

This script is designed for use with [Rotorz Tile System][RTS] by
providing a custom object factory which allows you to use
[PoolManager (by Path-o-logical Games)][PoolManager] to take advantage
of pooling for game objects that are instantiated from prefabs when
painting tiles.

Licensed under the MIT license. See LICENSE file in the project root for full license
information. DO NOT contribute to this project unless you accept the terms of the
contribution agreement.

**Minimum required version of Rotorz Tile System:** 2.0.2

[RTS]: http://rotorz.com/tilesystem/
[PoolManager]: http://poolmanager.path-o-logical.com/

## Installing script

Add the "RtsPoolManagerObjectFactory.cs" script somewhere within the
"Assets" folder of your project. For organisational purposes the
following path might be most appropriate:

    {Your Project}/Assets/RTS_Extras/PoolManager/

## Setting up your scene

1. Create a blank game object (**Game Object | Create Empty**).
1. Rename the game object to "RTS Pool".
1. Add a spawn pool to "RTS Pool" (**Component | Path-o-logical | PoolManager | SpawnPool**).
1. Add the `RTSPoolManagerObjectFactory` script to "RTS Pool" (**Component | RTS Extras | PoolManager | Object Factory**).
1. [Configure][ConfigureSpawnPool] the spawn pool component as needed.
1. Enjoy pooling with your tile systems!

[ConfigureSpawnPool]: http://docs.poolmanager.path-o-logical.com/home/per-prefab-options

## How does it work?

This script overrides the default runtime object factory for scenes
that have been setup using the above steps.

When tiles are painted any accompanying game objects are instantiated
via the object factory. The beauty of PoolManager is realized if the
prefab has been configured for the "RTS Pool" spawn pool; otherwise
the prefab is instantiated and destroyed in the usual way.

Likewise, when tiles are erased, their associated game object is
destroyed via the object factory.

There is no need to use the [`PreRuntimePoolItem`][PrePool] component
to enable pooling for tiles that are painted using the editor since
tiles are automatically added to the spawn pool upon being erased.
Just avoid adjusting the names of painted tiles!

The "RTS Pool" object is automatically positioned at the world
origin upon loading to avoid placement errors occurring when
tiles are reparented. It is generally better to avoid placing
"RTS Pool" within the hierarchy of another object; though if
you do, all parent objects will also be positioned at the world
origin.

[PrePool]: http://docs.poolmanager.path-o-logical.com/home/4-pooling-pre-runtime

## Caveat

Be careful to avoid using the same name for multiple prefabs when
taking advantage of pooling. This is because the prefab name is
used to connect game objects to their prefabs. Errors will occur
when same name is used for multiple prefabs.

## Useful links

- [Rotorz Tile System](http://rotorz.com/tilesystem)
- [PoolManager](http://poolmanager.path-o-logical.com/)
- [Rotorz Website](http://rotorz.com)
- [Path-o-logical Games Website](http://www.path-o-logical.com/)

Contribution Agreement
----------------------

This project is licensed under the MIT license (see LICENSE). To be in the best
position to enforce these licenses the copyright status of this project needs to
be as simple as possible. To achieve this the following terms and conditions
must be met:

- All contributed content (including but not limited to source code, text,
  image, videos, bug reports, suggestions, ideas, etc.) must be the
  contributors own work.

- The contributor disclaims all copyright and accepts that their contributed
  content will be released to the public domain.

- The act of submitting a contribution indicates that the contributor agrees
  with this agreement. This includes (but is not limited to) pull requests, issues,
  tickets, e-mails, newsgroups, blogs, forums, etc.