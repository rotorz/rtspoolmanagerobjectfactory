// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using PathologicalGames;
using Rotorz.Tile;
using UnityEngine;

[AddComponentMenu("RTS Extras/PoolManager/Object Factory")]
public class RtsPoolManagerObjectFactory : MonoBehaviour, IObjectFactory {

	// Cached reference to transform component of this object
	private Transform _transform;
	// Reference to the associated spawn pool component.
	private SpawnPool _pool;
	
	void Awake() {
		// Use this as the runtime object factory!
		DefaultRuntimeObjectFactory.Current = this;
		
		_transform = transform;

		// Pool manager object should be placed at world origin to
		// avoid placement errors which occur when re-parenting
		// tiles to a tile system.
		Transform t = _transform;
		while (t != null) {
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;

			// If pool manager is nested within an object hierarchy
			// it is important that each parent is also placed at
			// world origin in-order to avoid placement errors.
			t = t.parent;
		}

		// Fetch the associated spawn pool (or create it)
		_pool = GetComponent<SpawnPool>();
		if (_pool == null)
			_pool = gameObject.AddComponent<SpawnPool>();
	}
	
	#region IObjectFactory Implementation
	
	GameObject IObjectFactory.InstantiatePrefab(GameObject prefab, IObjectFactoryContext context) {
		if (prefab == null)
			return null;
		
		if (_pool.prefabPools.ContainsKey(prefab.name)) {
			Transform prefabTransform = prefab.transform;
			
			// Spawn prefab from pool
			Transform goTransform = _pool.Spawn(prefabTransform);
			if (goTransform == null)
				return null;
			
			// Restore transform from original prefab
			goTransform.localPosition = prefabTransform.localPosition;
			goTransform.localRotation = prefabTransform.localRotation;
			goTransform.localScale = prefabTransform.localScale;
			
			// Make sure that game object name matches prefab for lookup later!
			goTransform.name = prefab.name;
			
			return goTransform.gameObject;
		}
		else {
			// This prefab is not pooled, just create it in the usual way
			return Instantiate(prefab) as GameObject;
		}
	}
	
	void IObjectFactory.DestroyObject(GameObject go, IObjectFactoryContext context) {
		if (go == null)
			return;
		
		// Remove reference from tile data
		if (context.Tile != null)
			context.Tile.gameObject = null;
		
		if (_pool.prefabPools.ContainsKey(go.name)) {
			PrefabPool pool = _pool.prefabPools[go.name];
			Transform goTransform = go.transform;
			
			// Despawn! It is good to recycle :-)
			if (pool.spawned.Contains(goTransform))
				_pool.Despawn(goTransform);
			else if (!pool.despawned.Contains(goTransform))
				_pool.Add(goTransform, go.name, true, false);
		 // else
		 // 	Do nothing. Already in pool and already despawned

			// Place object back into pool container
			if (goTransform != null)
				goTransform.parent = _transform;
		}
		else {
			// This prefab is not pooled, just destroy as usual!
			Destroy(go);
		}
	}
	
	#endregion

}